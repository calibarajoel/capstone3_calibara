import React,{useContext} from 'react'

import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';

import {NavLink, Link} from 'react-router-dom'


import UserContext from '../userContext'


export default function NavBar(){

	

const {user,unsetUser,setUser} = useContext(UserContext)

function logout(){
	
	unsetUser()

	
	setUser({

		email:null,
		isAdmin:null

	})

	
	window.location.replace('/login')
}
	
	

	return (


		<Navbar className="color-nav" variant="light">
			<Navbar.Brand as={Link} to="/" >Sweetest Escape</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					<Nav.Link as={NavLink} to="/">Home</Nav.Link>
					<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
					{
						user.email
						? 
							user.isAdmin
							?	
								<>
									<Nav.Link as={NavLink} to="/addProduct">Add New Product</Nav.Link>
									<Nav.Link onClick={logout}>Logout</Nav.Link>
									{/*<Nav.Link as={NavLink} to="/updateProduct">Update Product</Nav.Link>*/}
								</>
							:
								<Nav.Link onClick={logout}>Logout</Nav.Link>
						: 
							<>
								<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
								<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
							</>
					}
					{/*<Nav.Link as={NavLink} to="/findProduct">Search</Nav.Link>	*/}
				</Nav>
			</Navbar.Collapse>
		</Navbar>


		)

}
