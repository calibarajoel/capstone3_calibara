import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';
//react-bootstrap components
import {Row,Col,Card} from 'react-bootstrap'


export default function Highlights(object){

	/*console.log(object)*/

	return (

		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>testimony1</h2>
							<img src="./image/remarks1.png" alt="testimony1"/>
						</Card.Title>
						<Card.Text>
							
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2 center>testimony2</h2>
							<img src="./image/testimony4.jpg" alt="testimony2"/>
						</Card.Title>
						<Card.Text>
							
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>testimony3</h2>
							<img src="./image/remarks3.png" alt="testimony3"/>
						</Card.Title>
						<Card.Text>
				
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>



		)


}
