import React,{useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

// bootstrap
import {Container} from 'react-bootstrap'

// dom 
import {BrowserRouter as Router} from 'react-router-dom'
import {Route,Switch} from 'react-router-dom'





// components
import NavBar from './components/NavBar'
import Banner from './components/Banner'
import Highlights from './components/Highlights'
import Product from './components/Product'
import Register from './pages/register'
import Login from './pages/login'
import Home from './pages/home'
import Products from './pages/products'
import NotFound from './pages/notFound'
import AddProduct from './pages/addProduct'
// import FindProduct from './pages/findProduct'
// import UpdateProduct from './pages/updateProduct'

import products from './data/productsData'

import {UserProvider} from './userContext'


function App() {
   


   const [user,setUser] = useState({

      email: localStorage.getItem('email'),
      isAdmin: localStorage.getItem('isAdmin') === "true"
    })

    console.log (user)

    function unsetUser(){
      localStorage.clear()
    }

    /*console.log(courseComponents)*///An array of Course components that return react elements.
    return(

        <>
            <UserProvider value={{user,setUser,unsetUser}}>
              <Router>
                <NavBar />
                <Container>
                   <Switch>
                     <Route exact path="/products" component={Products}/>
                     <Route exact path="/" component={Home}/>
                     <Route exact path="/register" component={Register} />
                     <Route exact path="/login" component={Login} />
                     <Route exact path="/addProduct" component={AddProduct}/>
                    {/* <Route exact path="/findProduct" component={findProduct}/>*/}
                   {/* { <Route exact path="/updateProduct" component={UpdateProduct}/>}*/}
                     <Route component={NotFound}/>
                   </Switch>
                </Container>
              </Router>
            </UserProvider>
        </>

      )

  }

  export default App; 

