import React, {useState,useEffect} from 'react'

import {Card,Button} from 'react-bootstrap'

export default function Product({productProp}){

	const [count,setCount] = useState(0)
	const [orders,setOrders] = useState(20)
	const [isActive,setIsActive] = useState(true)


	useEffect(()=>{

		if(orders === 0){
			setIsActive(false)
		}

	},[orders])

	
	function order(){

			setCount(count + 1)
			setOrders(orders - 1)

}
	return (

		<Card>
			<Card.Body>
				<Card.Title>
					<h2>{productProp.productName}</h2>
				</Card.Title>
				<Card.Text>
					{productProp.description}
				</Card.Text>
				<Card.Text>
					Price:&#8369;{productProp.price}
				</Card.Text>
				<Card.Text>
					Availability: {

						count === 0 

						? <span className="text-danger">Product Still Available.</span>

						: <span className="text-success">{count}</span>

					}
				</Card.Text>
				<Card.Text>
					Available Orders: {

						orders === 0

						? <span className="text-danger">No longer available for this day</span>
						: <span className="text-success">{orders}</span>

					}
				</Card.Text>
				{
					isActive === false

					? <Button variant="primary" disabled>Enroll</Button>
					: <Button variant="primary" onClick={order}>Order</Button>
				}
				
			</Card.Body>
		</Card>

		)	
};