import React,{useState,useEffect,useContext} from 'react'

import {Table,Button} from 'react-bootstrap'

/*components*/
import Banner from '../components/Banner'
import Product from '../components/Product'

import products from '../data/productsData'

import UserContext from '../userContext'

import {Redirect} from 'react-router-dom'

export default function Products(){

	//get the global user state from User Context.
	const {user} = useContext(UserContext)

	/*
		Mini-Activity

		Create a new array of Course components for each of the courses in our array.
			-You may use the .map() method to map out each item in the courses array into a Course component
		Then display this array of Course components in our return for the courses page

	*/
	//state to store our courses
	const [allProducts,setAllProducts] = useState([])
	const [activeProducts,setActiveProducts] = useState([])
	const [update,setUpdate] = useState(0)

	const [willRedirect,setWillRedirect] = useState(false)

	useEffect(()=>{

		//If your request is a get method, there is no need to indicate in an options, our method.
		//If your request is a get method, and there is no need to pass token, we do not have to include authorization in our headers.
		//If your request is a get method, there should be no body of the request, therefore no need to pass body in the options.
		//But if the get method request needs a token, pass an authorization header in the options.


		fetch('https://pure-lake-51850.herokuapp.com/api/products')
		.then(res => res.json())
		.then(data => {

			/*console.log(data)*/

			//all courses
			setAllProducts(data.data)

			let productsTemp = data.data

			/*temporary array to hold filtered items. only active courses*/
			let tempArray = productsTemp.filter(product => {

				//filter will return items that pass the condition into the new array.
				return product.isActive === true

			})

			//only active courses
			setActiveProducts(tempArray)


		})

	},[update])

	/*console.log(activeCourses)*/

	/*our course components should only show our active course*/
	let productComponents = activeProducts.map(product => {

		return (

			<Product key={product._id} productProp={product} />

			)

	})

	function archive(productId){

		fetch(`https://pure-lake-51850.herokuapp.com/api/products/archive/${productId}`,{

			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}

		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			/*refreshing page*/
			/*window.location.replace('/courses')*/
			/*re-render the component and re-run the useEffect which fetches our items*/
			setUpdate({})



		})

	}

/*	console.log(update)*/

	function activate(productId){

		fetch(`https://pure-lake-51850.herokuapp.com/api/products/activate/${productId}`,{

			method: 'PUT',
			headers: {

				'Authorization': `Bearer ${localStorage.getItem('token')}`

			}

		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			setUpdate({})


		})


	}

	console.log(typeof null)
	
	let productRows = allProducts.map(product => {

		

		return (

				<tr key={product._id}>
					<td>{product._id}</td>
					<td>{product.productName}</td>
					<td className={product.isActive ? "text-success": "text-danger"}>{product.isActive ? "Active":"Inactive"}</td>
					<td>
						{
							product.isActive
							?
							<Button variant="danger" className="mx-2" onClick={()=>archive(product._id)}>Archive</Button>
							:
							<Button variant="success" className="mx-2" onClick={()=>activate(product._id)}>Activate</Button>

						}
					</td>
				</tr>


			)

	})


	

	let bannerContent = {

		title: "Welcome to the product page",
		description: "Craving for Sweets? we got you!",
		label: "Please login to order",
		destination: "/login"

	}

	return(
		user.isAdmin === true
		?
		<>
			<h1 className="text-center">Admin Dashboard</h1>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{productRows}
				</tbody>
			</Table>
		</>
		:
		<>
			<Banner bannerProps={bannerContent}/>
			{productComponents}
		</>

		)

}
