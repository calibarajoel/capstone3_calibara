import React,{useState,useEffect,useContext} from 'react'

import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

import {Redirect} from 'react-router-dom'

import UserContext from '../userContext'

export default function AddProduct(){

	//get our global user state from our context
	const {user} = useContext(UserContext)

	//input states
	const [name,setName] = useState("")
	const [description,setDescription] = useState("")
	const [price,setPrice] = useState("")
	//state for our submit button conditional rendering
	const [isActive,setIsActive] = useState(true)


	//useEffect to check input and disable/enable the submit button
	useEffect(()=>{

		if(name !== "" && description !== "" && price !==""){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[name, description, price]) 

	/*get token from localStorage*/
/*	console.log(localStorage.getItem('token'))
	console.log(localStorage.token)*/
	
	function addProduct(e){

		e.preventDefault()

		let token = localStorage.getItem('token')

		fetch('https://pure-lake-51850.herokuapp.com/api/products/',{

			method: 'POST',
			headers: {
				//if your request has a body, you have to pass the content type header.
				'Content-Type': 'application/json',
				//if your request needs a token, you have to pass authorization header.
				'Authorization': `Bearer ${localStorage.getItem('token')}`

			},
			body: JSON.stringify({

				productName: name,
				description: description,
				price: price

			})
		})
		.then(res => res.json())
		.then(data => {

			
			if(data.message){
				Swal.fire({

					icon: "error",
					title: " Creation Failed.",
					text: data.message

				})
			} else {
				console.log(data)
				Swal.fire({

					icon: "success",
					title: "Product Creation Successful.",
					text: `Product has been created.`

				})
			}

		})


		setName("")
		setDescription("")
		setPrice("")

	}

	/*added ternary to conditionally render the form. Redirect the user if he is regular or a guest (not logged in)*/
	return (
		user.isAdmin === false
		?
		<Redirect to="/"/>
		:
		<>
			<h1>Create New Product</h1>
			<Form onSubmit={ e => addProduct(e)}>
				<Form.Group controlId="name">
					<Form.Label>Product Name:</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter Name"
						value={name}
						onChange={(e) => setName(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group controlId="description">
					<Form.Label>Description:</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter Description"
						value={description}
						onChange={(e) => setDescription(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group controlId="price">
					<Form.Label>Price:</Form.Label>
					<Form.Control 
						type= "number" 
						placeholder="Enter Description"
						value={price}
						onChange={(e) => setPrice(e.target.value)}
						required
					/>
				</Form.Group>
				{
					isActive 
					? <Button type="submit" variant="primary">Submit</Button>
					: <Button type="submit" variant="danger" disabled>Submit</Button>
				}
			</Form>
		</>

		)

}
