import React, {useState,useEffect, useContext} from 'react'

/*bootstrap components*/
import {Form,Button} from 'react-bootstrap'

/*sweetalert*/
import Swal from 'sweetalert2'

import UserContext from '../userContext'

//import redirect: this component allows redirection from one page to the other
import {Redirect} from 'react-router-dom'

export default function Register(){

	const {user,setUser} = useContext(UserContext)

	
	const [email,setEmail] = useState("")
	const [mobileNo,setMobileNo] = useState("")
	const [password,setPassword] = useState("")
	const [confirmPassword,setConfirmPassword] = useState("")

	/*conditional rendering for button*/
	const [isActive,setIsActive] = useState(false)
	const [willRedirect,setWillRedirect] = useState(false)



	useEffect(()=>{

		if((email !== "" && mobileNo !== "" &&password !== "" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)){

			setIsActive(true)

		} else {
			
			setIsActive(false)

		}

	},[email,mobileNo,password,confirmPassword])

	

	function registerUser(e){

		/*Submit event has a default behavior, for now, it will refresh the page and therefore, we lose our information*/
		e.preventDefault()
		console.log("The page will no longer refresh because of submit.")
	

		fetch('https://pure-lake-51850.herokuapp.com/api/register',{

			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				email: email,
				mobileNo: mobileNo,
				password: password

			})

		//response should be parsed into an object to be used in javascript, that is with the use of .json()
		})
		.then(response => response.json())
		.then(data => {

			//actual response from the server/api
			console.log(data)
			
			if(data.message){

				Swal.fire({

					icon: "error",
					title: "Registration Failed.",
					text: data.message

				})

			} else {

				Swal.fire({

					icon: "success",
					title: "Registration Successful!",
					text: "Thank you for registering."

				})

				//update the willRedirect state to true after user registers successfully.
				setWillRedirect(true)

			}

		})
	
		setEmail("")
		setMobileNo("")
		setPassword("")
		setConfirmPassword("")

	}
	return (
		user.email || willRedirect
		?
		<Redirect to="/" />
		:
		<Form onSubmit={e=>registerUser(e)}>
			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e=>{setEmail(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Mobile Number:</Form.Label>
				<Form.Control type="number" placeholder="Enter 11 Digit Mobile No." value={mobileNo} onChange={e=>{setMobileNo(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e=>{setPassword(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Confirm Password:</Form.Label>
				<Form.Control type="password" placeholder="Confirm Password" value={confirmPassword} onChange={e=>{setConfirmPassword(e.target.value)}} required/>
			</Form.Group>
			{
				isActive
				? <Button variant="primary" type="submit">Submit</Button>
				: <Button variant="primary" disabled>Submit</Button>
			}
			
		</Form>
		)

}
