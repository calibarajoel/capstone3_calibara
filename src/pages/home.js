import React from 'react'
import '../App.css'
import 'bootstrap/dist/css/bootstrap.min.css';

import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
export default function Home(){


	let bannerContent = {

	  title: "Sweetest Escape",
	  description: "Craving for Sweets? we got you!",
	  label: "Order now!",
	  destination: "/products"

	}

	return (
		<>
			<Banner bannerProps={bannerContent}/>
			<Highlights/>
		</>

		)

}

