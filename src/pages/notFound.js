import React from "react"

import Banner from '../components/Banner'

export default function NotFound(){

	let bannerContent = {

	  title: "Page not found",
	  description: "Craving for Sweets? we got you!",
	  label: "Get me Back",
	  destination: "/"
	 }
 

	return (
		<Banner bannerProps={bannerContent}/>
		)
}