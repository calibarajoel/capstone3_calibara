import React from 'react'
import '../App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Jumbotron,Button,} from 'react-bootstrap'
// import background from "../images/bg1.png"

import {Link} from 'react-router-dom'



export default function Banner({bannerProps}){

	
	
	return (
	
			<div className="container">
				<Jumbotron>
					<h1>{bannerProps.title}</h1>
					<p>{bannerProps.description}</p>
					<Link to={bannerProps.destination} className="btn btn-primary">{bannerProps.label}</Link>
				</Jumbotron>		
			</div>
		
		)

}
